#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "MenuScene.h"
#include "configurator\Configurator.h"
#include "../gameClasses/Bird.h"
#include "../gameClasses/CloudGenerator.h"
#include "../gameClasses/BirdWaveGenerator.h"

USING_NS_CC;

Scene* GameScene::createScene() {
    // 'scene' is an autorelease object
    auto scene = Scene::create();    
    // 'layer' is an autorelease object
    auto layer = GameScene::create();
	// add layer as a child to scene
    scene->addChild(layer);
	// return the scene
    return scene;
}

bool GameScene::init() {

    if ( !LayerGradient::initWithColor(Color4B(155, 238, 255, 255), Color4B(63, 184, 255, 255))){
        return false;
    }
	CCLOG("LAST PTS %d", config::Configurator::getInstance()->getPoints());
	config::Configurator::getInstance()->setPoints(0);

	auto cloudGenerator = CloudGenerator::create();
	cloudGenerator->generateWave(8);
	addChild(cloudGenerator);

	auto birdWaveGenerator = BirdWaveGenerator::create();
	birdWaveGenerator->gameLoop();
	addChild(birdWaveGenerator);

	addChild(config::Configurator::getInstance()->initLabel());
    return true;
}

void GameScene::onTapEnded(cocos2d::Touch* touch, cocos2d::Event* event) {
	auto action = MoveTo::create(0.2, Vec2(touch->getLocation().x, touch->getLocation().y));
	GameScene::mySprite->runAction(action);
}

bool GameScene::onTapBegan(cocos2d::Touch* touch, cocos2d::Event* event) {
	return true;
}

void GameScene::menuCloseCallback(Ref* pSender) {
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
